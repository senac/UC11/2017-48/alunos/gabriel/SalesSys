package br.com.senac.salessys.model;


public class Cliente {

    private String nome;
    private String cidade;
    private String estado;
    private String profissao;
    private String nomeEmpresa;
    private String email;
    private String telefone;
    private String obsavacoes;

    public Cliente() {
    }

    public Cliente(String nome, String cidade, String estado, String profissao, String nomeEmpresa, String email, String obsavacoes, String telefone) {
        this.nome = nome;
        this.cidade = cidade;
        this.estado = estado;
        this.profissao = profissao;
        this.nomeEmpresa = nomeEmpresa;
        this.email = email;
        this.obsavacoes = obsavacoes;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObsavacoes() {
        return obsavacoes;
    }

    public void setObsavacoes(String obsavacoes) {
        this.obsavacoes = obsavacoes;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
