package br.com.senac.salessys.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.salessys.R;
import br.com.senac.salessys.model.Cliente;

public class ClienteActivity extends AppCompatActivity {

    private List<String> listaEstados = new ArrayList<>();

    private Button btnSalvar;

    private EditText txtNomeCopleto;
    private EditText txtNomeEmpresa;
    private EditText txtTelefone;
    private EditText txtEmail;
    private EditText txtProfissao;
    private EditText txtObs;

    private Spinner spiEstados ;
    private Spinner spiCidade ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        spiEstados = findViewById(R.id.spiUF);
        spiCidade = findViewById(R.id.spiCidade);

        txtNomeCopleto = findViewById(R.id.txtNomeCompleto);
        txtNomeEmpresa = findViewById(R.id.txtNomeEmpresa);
        txtTelefone = findViewById(R.id.txtTelefone);
        txtEmail = findViewById(R.id.txtEmail);
        txtProfissao = findViewById(R.id.txtProfissao);
        txtObs = findViewById(R.id.txtObs);

        btnSalvar = findViewById(R.id.btnSalvar);


        ArrayAdapter<CharSequence>  adapterEstados = ArrayAdapter.createFromResource(this, R.array.listaEstados, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence>  adapterCidade = ArrayAdapter.createFromResource(this, R.array.listaCidades, android.R.layout.simple_spinner_item);


        spiEstados.setAdapter(adapterEstados);
        spiCidade.setAdapter(adapterCidade);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nomeCompleto = txtNomeCopleto.getText().toString();
                String nomeEmpresa = txtNomeEmpresa.getText().toString();
                String telefone = txtTelefone.getText().toString();
                String email = txtEmail.getText().toString();
                String profissao = txtProfissao.getText().toString();
                String obs = txtObs.getText().toString();
                String estado = (String) spiEstados.getSelectedItem();
                String cidade = (String) spiCidade.getSelectedItem();

                Cliente cliente = new Cliente(nomeCompleto, cidade, estado,profissao, nomeEmpresa,email,obs,telefone);



            }
        });
    }

}
